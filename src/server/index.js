// 1. Number of matches played per year for all the years in IPL.
function matches_played(json_array,deliveries_array)
{
    let matches_played_per_year = {};
    for(let index=0; index<json_array.length; index++)
    {
        if (matches_played_per_year.hasOwnProperty(json_array[index]["season"]))
        {
            matches_played_per_year[json_array[index]["season"]] += 1;
        }
        else
        {
            matches_played_per_year[json_array[index]["season"]] = 1;
        }
    }
    
    return matches_played_per_year;
}

// 2. Number of matches won per team per year in IPL.
function matches_won(json_array, deliveries_array)
{
    let matches_won_per_year = {};
    for(let index=0; index<json_array.length; index++)
    {
        let season = json_array[index]["season"];
        let winner = json_array[index]["winner"];
    
        if(matches_won_per_year.hasOwnProperty(season))
        {
            if (matches_won_per_year[season].hasOwnProperty(winner))
            {
                matches_won_per_year[season][winner] += 1;
            }
            else
            {
                matches_won_per_year[season][winner] = 1;
            }
        }   
        else
        {
            matches_won_per_year[season] = {};
            matches_won_per_year[season][winner] = 1;
        }
    }
    return matches_won_per_year;
}

// 3.Extra runs conceded per team in the year 2016
function extra_runs(json_array, deliveries_array)
{
    let extra_runs_per_team = {};
    for(let index=0; index<json_array.length; index++)
    {
        if(json_array[index]["season"] == 2016)
        {
            let id = json_array[index]["id"];
            for(let ind=0; ind<deliveries_array.length; ind++)
            {
                if(deliveries_array[ind]["match_id"] == id)
                {
                    let team1 = deliveries_array[ind]["batting_team"];
                    let team2 = deliveries_array[ind]["bowling_team"];
                    if(deliveries_array[ind]["batting_team"] == team1)
                    {
                        if(extra_runs_per_team.hasOwnProperty(team1))
                        {
                            extra_runs_per_team[team1] += parseInt(deliveries_array[ind]["extra_runs"])
                        }
                        else
                        {
                            extra_runs_per_team[team1] = parseInt(deliveries_array[ind]["extra_runs"])
                        }
                    }
                    else
                    {
                        if(extra_runs_per_team.hasOwnProperty(team2))
                        {
                            extra_runs_per_team[team2] += parseInt(deliveries_array[ind]["extra_runs"]);
                        }
                        else
                        {
                            extra_runs_per_team[team2] = parseInt(deliveries_array[ind]["extra_runs"]);
                        }
                    }
                }
            }
        }
    }
    return extra_runs_per_team;
}


// 4.Top 10 economical bowlers in the year 2015
function top_economical_bowlers(json_array, deliveries_array)
{
    let bowlers_economy = {};
    for(let index=0; index<json_array.length; index++)
    {
        let year = json_array[index]["season"];
        if(year == 2015)
        {
            let match_id =json_array[index]["id"];
            for( let ind=0; ind<deliveries_array.length; ind++)
            {
                if(match_id == deliveries_array[ind]["match_id"])
                {
                    let bowler = deliveries_array[ind]["bowler"];
                    let runs = deliveries_array[ind]["total_runs"];
                    if(bowlers_economy.hasOwnProperty(bowler))
                    {
                        bowlers_economy[bowler]["total_runs"] += parseInt(runs); 
                        bowlers_economy[bowler]["balls"] += 1;
                    }
                    else
                    {
                        bowlers_economy[bowler]={};
                        bowlers_economy[bowler]["total_runs"] = parseInt(runs);
                        bowlers_economy[bowler]["balls"] = 1;
                    }
                }
            }
        }
    }
    let all_bowlers_economy = {};
    for(let prop in bowlers_economy)
    {
        let economy = parseFloat(bowlers_economy[prop]["total_runs"] / (bowlers_economy[prop]["balls"] / 6)).toFixed(2);
        all_bowlers_economy[prop] = economy;
    }
    let result = Object.entries(all_bowlers_economy).sort((a,b) => a[1]-b[1]);
    return result.slice(0,10);
}

// 5.Find the number of times each team won the toss and also won the match
function both_match_and_toss_win(json_array)
{
    let teams = {};
    for (let index=0; index<json_array.length; index++)
    {
        let toss_winner = json_array[index]["toss_winner"];
        let winner = json_array[index]["winner"]
        if(toss_winner == winner)
        {
            if(teams.hasOwnProperty(winner))
            {
                teams[winner] += 1;
            }
            else
            {
                teams[winner] = 1;
            }
        }
    }
    return teams;
}

// 6.Find a player who has won the highest number of Player of the Match awards for each season
function man_of_match(json_array,deliveries_array)
{
    let result = {};
    let players = {};
    for(let index=0; index<json_array.length; index++)
    {
        let player_of_match = json_array[index]["player_of_match"];
        let year = json_array[index]["season"];
        if(players.hasOwnProperty(year))
        {
            if(players[year].hasOwnProperty(player_of_match))
            {
                players[year][player_of_match] += 1;
            }
            else
            {
                players[year][player_of_match] = 1
            }
        }
        else
        {
            players[year] = {};
            players[year][player_of_match] = 1;
        }
    }
    for(let item in players)
    {
        let sorted_item = Object.entries(players[item]).sort((a,b) => b[1]-a[1]);
        result[item] = sorted_item[0];
    }
    return result;
}

// 7.Find the strike rate of a batsman for each season
function strike_rate(batsman,json_array,deliveries_array)
{
    let strikeRate = {};
    for(let index=0; index<deliveries_array.length; index++)
    {
        let player = deliveries_array[index]["batsman"];
        let year = 0;
        if(player == batsman)
        {
            let id = deliveries_array[index]["match_id"];
            for(let ind=0; ind<json_array.length; ind++)
            {
                if(id == json_array[ind]["id"])
                {
                    year = json_array[ind]["season"];
                    break;
                }
            }
            if(strikeRate.hasOwnProperty(year))
            {
                strikeRate[year]["runs"] += parseInt(deliveries_array[index]["batsman_runs"]);
                strikeRate[year]["balls"] += 1;
            }
            else
            {
                strikeRate[year] = {};
                strikeRate[year]["runs"] = parseInt(deliveries_array[index]["batsman_runs"]);
                strikeRate[year]["balls"] = 1;
            }
        }
    }
    let results = {};
    for(let item in strikeRate)
    {
        let strike = parseFloat((strikeRate[item]["runs"] * 100) / strikeRate[item]["balls"]).toFixed(2);
        results[item] = strike;
    }
    return results;
}
// 8.Find the highest number of times one player has been dismissed by another player.
function dismissal(json_array,deliveries_array)
{
    let players = {};
    for(let index=0; index<deliveries_array.length; index ++)
    {
        let player = deliveries_array[index]["player_dismissed"];
        if(players.hasOwnProperty(player))
        {
            players[player] +=1;
        }
        else
        {
            players[player] = 1;
        }
    }
    let result = Object.entries(players).sort((a,b) => b[1]-a[1]);
    return result[1];
}

// 9.Find the bowler with the best economy in super overs
function superOver(json_array, deliveries_array)
{
    let best_economy = {};
    for(let index=0; index<deliveries_array.length; index++)
    {
        let super_over = parseInt(deliveries_array[index]["is_super_over"]);
        if(super_over)
        {
            let bowler = deliveries_array[index]["bowler"];
            let total_runs = parseInt(deliveries_array[index]["total_runs"]);
            if(best_economy.hasOwnProperty(bowler))
            {
                best_economy[bowler]["total_runs"] += total_runs
                best_economy[bowler]["balls"] += 1;
            }
            else
            {
                best_economy[bowler] = {};
                best_economy[bowler]["total_runs"] = total_runs;
                best_economy[bowler]["balls"] = 1;
            }
        }
    }
    let results ={};
    for(let obj in best_economy)
    {
        let overs = parseFloat(best_economy[obj]["balls"]/6).toFixed(2);
        let economy = parseFloat(best_economy[obj]["total_runs"] / overs).toFixed(2); 
        results[obj] = economy; 
    }
    let final_result = Object.entries(results).sort((a,b)=> a[1]-b[1]);
    return final_result[0];
}

exports.matches_played = matches_played;
exports.matches_won = matches_won;
exports.extra_runs = extra_runs;
exports.top_economical_bowlers = top_economical_bowlers;
exports.both_match_and_toss_win = both_match_and_toss_win;
exports.man_of_match = man_of_match;
exports.dismissal = dismissal;
exports.superOver = superOver;
exports.strike_rate = strike_rate;