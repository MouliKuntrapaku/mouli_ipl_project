// importing modules
const csvjson = require('csvjson');
const fs = require('fs');
const path = require('path');
const IPL = require("./index.js");

// converting Matches.csv to json_array
const data = fs.readFileSync(path.join(__dirname, '../data/matches.csv'), { encoding : 'utf8'});
const options = {
  delimiter : ',', 
  quote     : '"' 
};
const json_array = csvjson.toObject(data, options);

// converting Deliveries.csv to deliveries_array
const data1 = fs.readFileSync(path.join(__dirname, '../data/deliveries.csv'), { encoding : 'utf8'});
const options1 = {
  delimiter : ',', 
  quote     : '"' 
};
const deliveries_array = csvjson.toObject(data1, options1);

let MatchesPlayedd = IPL.matches_played(json_array,deliveries_array);
let MatchesWonn = IPL.matches_won(json_array,deliveries_array);
let ExtraRunss = IPL.extra_runs(json_array,deliveries_array);
let TopTenEconomy = IPL.top_economical_bowlers(json_array,deliveries_array);
let BothMatchTossWin = IPL.both_match_and_toss_win(json_array);
let ManOfMatchh = IPL.man_of_match(json_array,deliveries_array);
let StrikeRate = IPL.strike_rate("CH Gayle",json_array,deliveries_array);
let Dismissalss = IPL.dismissal(json_array,deliveries_array);
let superOverr = IPL.superOver(json_array,deliveries_array);

// parsing Output to json files
fs.writeFile("../public/output/BestEconomy_SuperOver.json",JSON.stringify(superOverr) , (err) => {
  console.log("Generated the bowler with the best economy in super overs")})

fs.writeFile("../public/output/BothMatchAndTossWin.json",JSON.stringify(BothMatchTossWin) , (err) => {
  console.log("Generated the number of times each team won the toss and also won the match")})

fs.writeFile("../public/output/extraRunsPerTeam.json",JSON.stringify(ExtraRunss) , (err) => {
  console.log("Generated Extra runs conceded per team in the year 2016")})

fs.writeFile("../public/output/HighestManOfMatches.json",JSON.stringify(ManOfMatchh) , (err) => {
  console.log("Generated a player who has won the highest number of Player of the Match awards for each season")})

fs.writeFile("../public/output/matchesPerYear.json",JSON.stringify(MatchesPlayedd) , (err) => {
  console.log("Generated Number of matches played per year for all the years in IPL.")})

fs.writeFile("../public/output/matchesWonPerTeam.json",JSON.stringify(MatchesWonn) , (err) => {
  console.log("Generated Number of matches won per team per year in IPL.")})

fs.writeFile("../public/output/MaxmDismissals.json",JSON.stringify(Dismissalss) , (err) => {
  console.log("Generated the highest number of times one player has been dismissed by another player")})

fs.writeFile("../public/output/StrikeRate.json",JSON.stringify(StrikeRate) , (err) => {
  console.log("Generated the strike rate of a batsman for each season")})

fs.writeFile("../public/output/TopEconomicalBowlers.json",JSON.stringify(TopTenEconomy) , (err) => {
  console.log("Generated Top 10 economical bowlers in the year 2015")})